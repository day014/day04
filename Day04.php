<!DOCTYPE html>
<html>
<head>
    <title>Đăng ký</title>
    <style>
        .error-text {
            color: red;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>

<h2>Form Đăng Ký</h2>

<div id="error-message" class="error-text"></div>

<form id="registration-form">
    <label for="name" class="error-text">Hãy nhập tên.</label>
    <br>
    <label for="dob" class="error-text">Hãy nhập ngày sinh.</label>
    <br>
    Họ và tên*: <input type="text" id="fullName" name="fullName"><br><br>
    Giới tính*:
    <input type="radio" id="male" name="gender" value="male"> Nam
    <input type="radio" id="female" name="gender" value="female"> Nữ
    <label for="gender" class="error-text">Hãy chọn giới tính.</label>
    <br><br>
    Phân khoa*:
    <select id="department" name="department">
        <option value="" disabled selected>--Chọn phân khoa--</option>
        <option value="department1">Phân khoa 1</option>
        <option value="department2">Phân khoa 2</option>
    </select>
    <label for="department" class="error-text">Hãy chọn phân khoa.</label>
    <br><br>
    Ngày sinh*:
    <input type="text" id="dob" name="dob" placeholder="dd/mm/yyyy">
    <label for="dob" class="error-text">Yêu cầu mục “Ngày sinh” cần nhập đúng định dạng dd/mm/yyyy.</label>
    <br><br>
    Địa chỉ : <input type="text" id="address" name="address"><br><br>
    <button type="button" onclick="validateForm()">Đăng ký</button>
</form>

<script>
    function validateForm() {
        var name = $("#fullName").val();
        var dob = $("#dob").val();
        var department = $("#department").val();

        $("#error-message").html(""); // Clear previous error messages

        if (name === "") {
            $("#error-message").append("Hãy nhập tên.<br>");
        }

        if (department === null) {
            $("#error-message").append("Hãy chọn phân khoa.<br>");
        }

        if (dob !== "") {
            var datePattern = /^(\d{2})\/(\d{2})\/(\d{4})$/;
            if (!datePattern.test(dob)) {
                $("#error-message").append("Hãy nhập ngày sinh đúng định dạng dd/mm/yyyy.<br>");
            }
        }

        // Scroll to the top of the form to show the error messages
        $('html, body').animate({scrollTop: $("#registration-form").offset().top}, 'slow');
    }
</script>

</body>
</html>
